unit UnitFormMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Math, Contnrs, StdCtrls, ShellAPI, ImgList, ComCtrls,
  IniFiles, Buttons, Types, StrUtils,
  UnitModels;

const
  WM_ICONTRAY = WM_USER + 1;

type
  TFormMain = class(TForm)
    TimerRepos: TTimer;
    ListViewWindows: TListView;
    EditClassWords: TEdit;
    Label1: TLabel;
    EditTitleWords: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    ComboBoxMonitor: TComboBox;
    Label4: TLabel;
    ComboBoxPosition: TComboBox;
    ButtonNew: TButton;
    ButtonDelete: TButton;
    ButtonTerminate: TButton;
    ButtonLookup: TButton;
    ButtonApply: TButton;
    procedure TimerReposTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ButtonNewClick(Sender: TObject);
    procedure ListViewWindowsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButtonTerminateClick(Sender: TObject);
    procedure ButtonDeleteClick(Sender: TObject);
    procedure EditClassWordsChange(Sender: TObject);
    procedure ListViewWindowsData(Sender: TObject; Item: TListItem);
    procedure EditTitleWordsChange(Sender: TObject);
    procedure ComboBoxMonitorChange(Sender: TObject);
    procedure ComboBoxPositionChange(Sender: TObject);
    procedure ButtonLookupClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ButtonApplyClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    TrayIconData: TNotifyIconData;
    KnownWindows: TObjectList;
    LoadingEdits: Integer;
    Lookuping: Boolean;
    WindowLayout: TWindowLayout; 
    procedure TrayMessage(var Msg: TMessage);  message WM_ICONTRAY;
    procedure LoadKnownWindows;
    procedure SaveKnownWindows;
    procedure EnableEdits(State: Boolean);
    procedure FillMonitors;
    procedure FillPositions;
    procedure LoadEdits(Known: TKnownWindow);
    procedure SaveEdits(Known: TKnownWindow);
    procedure ClearEdits;
    function SelectedKnown: TKnownWindow;
    function DetectMonitorOfWindow(Win: HWND): Integer;
    function DetectPositionOfWindow(Mon: Integer; Win: HWND): Integer;
    procedure Repos;
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

procedure GetWinStrs(hHwnd: HWND; var Cls, Title: string);
begin
  SetLength(Cls, 255);
  SetLength(Cls, GetClassName(hHwnd, PChar(Cls), Length(Cls)));
  SetLength(Title, 255);
  SetLength(Title, GetWindowText(hHwnd, PChar(Title), Length(Title)));
end;

function EnumWindowsHandler(hHwnd: HWND; lParam: Integer): Boolean; stdcall;
var
  Cls, Title: string;
  List: TStringList;
begin
  if hHwnd = NULL then
    Result := False
  else
  begin
    GetWinStrs(hHwnd, Cls, Title);
    List := TStringList(lParam);
    List.AddObject(Cls + '|' + Title, Pointer(hHwnd));
    Result := True;
  end;
end;

procedure Split(S: string; var Cls, Title: string);
var
  I: Integer;
begin
  I := Pos('|', S);
  Cls := Copy(S, 1, I - 1);
  Title := Copy(S, I + 1, MaxInt);
end;

function Match(S: string; List: array of string): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Length(List) - 1 do
    if Pos(List[I], S) = 0 then
      Exit;
  Result := True;
end;

function Find(ClsList, TitList: TStringDynArray): HWND;
var
  Cls, Title: string;
  List: TStringList;
  I: Integer;
begin
  List := TStringList.Create;
  try
    EnumWindows(@EnumWindowsHandler, Integer(List));
    for I := 0 to List.Count - 1 do
    begin
      Split(List[I], Cls, Title);
      if Match(Cls, ClsList) and Match(Title, TitList) then
      begin
        Result := HWND(List.Objects[I]);
        Exit;
      end;
    end;
  finally
    List.Free;
  end;
  Result := 0;
end;

function IsMaximized(Win: HWND): Boolean;
var
  Place: TWindowPlacement;
begin
  GetWindowPlacement(Win, @Place);
  Result := Place.showCmd = SW_MAXIMIZE;
end;

function IsActive(Win: HWND): Boolean;
begin
  Result := (Win = GetActiveWindow) or (Win = GetForegroundWindow);
end;

procedure SplitStr(S: string; var Parts: TStringDynArray);
var
  I, I0, Count: Integer;
begin
  if Trim(S) = '' then
  begin
    SetLength(Parts, 0);
    Exit;
  end;
  SetLength(Parts, 255);
  Count := 0;
  I0 := 1;
  while I0 <= Length(S) do
  begin
    I := PosEx('|', S, I0);
    if I = 0 then
      I := Length(S) + 1;
    Parts[Count] := Copy(S, I0, I - I0);
    Inc(Count);
    I0 := I + 1;
  end;
  SetLength(Parts, Count);
end;

procedure TFormMain.Repos;
var
  WinFound: HWND;
  MonitorRect, WinRect: TRect;
  Flags, I: Integer;
  Cell: TWindowLayoutCell;
  Known: TKnownWindow;
  ClassWords, TitleWords: TStringDynArray;
begin
  for I := 0 to KnownWindows.Count - 1 do
  begin
    Known := KnownWindows[I] as TKnownWindow;
    SplitStr(Known.ClassWords, ClassWords);
    SplitStr(Known.TitleWords, TitleWords);
    WinFound := Find(ClassWords, TitleWords);
    if WinFound <> 0 then
      if (Known.Monitor >= 0) and (Known.Monitor < Screen.MonitorCount) then
      begin
        MonitorRect := Screen.Monitors[Known.Monitor].WorkareaRect;
        if (Known.Position >= 0) and (Known.Position < WindowLayout.CellCount) then
        begin
          Cell := WindowLayout.Cells[Known.Position];
          if IsMaximized(WinFound) or IsActive(WinFound) then
          else
          begin
            WinRect := Cell.Rect(MonitorRect);
            Flags := SWP_NOZORDER or SWP_NOACTIVATE or SWP_NOREDRAW;
            SetWindowPos(WinFound, 0, WinRect.Left, WinRect.Top, WinRect.Right - WinRect.Left, WinRect.Bottom - WinRect.Top, Flags);
          end;
        end;
      end;
  end;
end;

function TFormMain.DetectMonitorOfWindow(Win: HWND): Integer;
var
  WinRect, MonitorRect, Foo: TRect;
  I: Integer;
begin
  GetWindowRect(Win, WinRect);
  for I := 0 to Screen.MonitorCount - 1 do
  begin
    MonitorRect := Screen.Monitors[I].WorkareaRect;
    if IntersectRect(Foo, MonitorRect, WinRect) then
    begin
      Result := I;
      Exit;
    end;
  end;
  Result := -1;
end;

function TFormMain.DetectPositionOfWindow(Mon: Integer; Win: HWND): Integer;
var
  WinRect, MonitorRect: TRect;
  Cell: TWindowLayoutCell;
begin
  GetWindowRect(Win, WinRect);
  MonitorRect := Screen.Monitors[Mon].WorkareaRect;
  Cell := WindowLayout.BestCellForRect(MonitorRect, WinRect);
  Result := Cell.Index;
end;

procedure TFormMain.TimerReposTimer(Sender: TObject);
var
  Win: HWND;
  Cls, Title: string;
  Mon, Pos: Integer;
begin
  if Lookuping then
  begin
    Win := GetForegroundWindow;
    if (Win <> 0) and (Win <> Self.Handle) then
    begin
      GetWinStrs(Win, Cls, Title);
      EditClassWords.Text := Cls;
      EditTitleWords.Text := Title;
      Mon := DetectMonitorOfWindow(Win);
      if Mon <> -1 then
      begin
        Pos := DetectPositionOfWindow(Mon, Win);
        ComboBoxMonitor.ItemIndex := Mon;
        ComboBoxPosition.ItemIndex := Pos;
        Self.Activate;
      end;
    end;
  end
  else if not Self.Visible then
    Repos;
end;

procedure TFormMain.EnableEdits(State: Boolean);
const
  EnableColors: array[Boolean] of TColor = (clSilver, clWhite);
begin
  EditClassWords.ReadOnly := not State;
  EditClassWords.Color := EnableColors[State];
  EditTitleWords.ReadOnly := not State;
  EditTitleWords.Color := EnableColors[State];
  ComboBoxMonitor.Enabled := State;
  ComboBoxMonitor.Color := EnableColors[State];
  ComboBoxPosition.Enabled := State;
  ComboBoxPosition.Color := EnableColors[State];
  ButtonLookup.Enabled := State;
  ButtonApply.Enabled := State;
  ButtonDelete.Enabled := State;
end;

procedure TFormMain.LoadKnownWindows;
var
  Ini: TMemIniFile;
  Count, I: Integer;
  Known: TKnownWindow;
  Sess: string;
begin
  Ini := TMemIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
  try
    Count := Ini.ReadInteger('Settings', 'WindowCount', 0);
    for I := 0 to Count - 1 do
    begin
      Known := TKnownWindow.Create;
      KnownWindows.Add(Known);
      Sess := 'Window' + IntToStr(I);
      Known.ClassWords := Ini.ReadString(Sess, 'ClassWords', '');
      Known.TitleWords := Ini.ReadString(Sess, 'TitleWords', '');
      Known.Monitor := Ini.ReadInteger(Sess, 'Monitor', 0);
      Known.Position := Ini.ReadInteger(Sess, 'Position', 0);
    end;
  finally
    Ini.Free;
  end;
  ListViewWindows.Items.Count := KnownWindows.Count;
  ListViewWindows.Refresh;
end;

procedure TFormMain.SaveKnownWindows;
var
  Ini: TMemIniFile;
  I: Integer;
  Known: TKnownWindow;
  Sess: string;
begin
  Ini := TMemIniFile.Create(ChangeFileExt(ParamStr(0), '.ini'));
  try
    Ini.WriteInteger('Settings', 'WindowCount', KnownWindows.Count);
    for I := 0 to KnownWindows.Count - 1 do
    begin
      Known := KnownWindows[I] as TKnownWindow;
      Sess := 'Window' + IntToStr(I);
      Ini.WriteString(Sess, 'ClassWords', Known.ClassWords);
      Ini.WriteString(Sess, 'TitleWords', Known.TitleWords);
      Ini.WriteInteger(Sess, 'Monitor', Known.Monitor);
      Ini.WriteInteger(Sess, 'Position', Known.Position);
    end;
    Ini.UpdateFile;
  finally
    Ini.Free;
  end;
end;

procedure TFormMain.FillMonitors;
var
  I: Integer;
begin
  ComboBoxMonitor.Items.Clear;
  for I := 0 to Screen.MonitorCount - 1 do
    ComboBoxMonitor.Items.Add('Mon #' + IntToStr(I));
end;

procedure TFormMain.FillPositions;
var
  I: Integer;
begin
  ComboBoxPosition.Items.Clear;
  for I := 0 to WindowLayout.CellCount - 1 do
    ComboBoxPosition.Items.Add(WindowLayout.Cells[I].Name);
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
  LoadingEdits := 0;
  Lookuping := False;
  KnownWindows := TObjectList.Create;
  WindowLayout := NewLayout3;

  FillMonitors;
  FillPositions;
  LoadKnownWindows;
  EnableEdits(False);
  ClearEdits;

  Position := poDefaultPosOnly;
  with TrayIconData do
  begin
    cbSize := SizeOf(TrayIconData);
    Wnd := Handle;
    uID := 0;
    uFlags := NIF_MESSAGE + NIF_ICON + NIF_TIP;
    uCallbackMessage := WM_ICONTRAY;
    hIcon := Application.Icon.Handle;
    StrPCopy(szTip, Application.Title);
  end;
  Shell_NotifyIcon(NIM_ADD, @TrayIconData);
end;

procedure TFormMain.FormDestroy(Sender: TObject);
begin
  Shell_NotifyIcon(NIM_DELETE, @TrayIconData);
end;

procedure TFormMain.TrayMessage(var Msg: TMessage);
begin
  case Msg.lParam of
    WM_LBUTTONDOWN: Self.Visible := not Self.Visible;
  end;
end;

procedure TFormMain.ButtonNewClick(Sender: TObject);
var
  Known: TKnownWindow;
begin
  Known := TKnownWindow.Create;
  KnownWindows.Add(Known);
  Known.Monitor := 0;
  Known.Position := 0;
  ListViewWindows.Items.Count := KnownWindows.Count;
  ListViewWindows.ItemIndex := KnownWindows.IndexOf(Known);
  ListViewWindows.Refresh;
end;

procedure TFormMain.LoadEdits(Known: TKnownWindow);
begin
  Inc(LoadingEdits);
  EditClassWords.Text := Known.ClassWords;
  EditTitleWords.Text := Known.TitleWords;
  ComboBoxMonitor.ItemIndex := Known.Monitor;
  ComboBoxPosition.ItemIndex := Known.Position;
  Dec(LoadingEdits);
end;

procedure TFormMain.ClearEdits;
begin
  Inc(LoadingEdits);
  EditClassWords.Clear;
  EditTitleWords.Clear;
  ComboBoxMonitor.ItemIndex := -1;
  ComboBoxPosition.ItemIndex := -1;
  Dec(LoadingEdits);
end;

procedure TFormMain.SaveEdits(Known: TKnownWindow);
begin
  if Known = nil then
    Exit;
  Known.ClassWords := EditClassWords.Text;
  Known.TitleWords := EditTitleWords.Text;
  Known.Monitor := ComboBoxMonitor.ItemIndex;
  Known.Position := ComboBoxPosition.ItemIndex;
  ListViewWindows.Refresh;
end;

procedure TFormMain.ListViewWindowsSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
var
  Known: TKnownWindow;
begin
  if Item = nil then
  begin
    ClearEdits;
    EnableEdits(False);
  end
  else
  begin
    Known := KnownWindows[Item.Index] as TKnownWindow;
    LoadEdits(Known);
    EnableEdits(True);
    EditClassWords.SetFocus;
  end;
end;

procedure TFormMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveKnownWindows;
  if Tag = 1 then
    Exit;
  Action := caNone;
  Self.Hide;
end;

procedure TFormMain.ButtonTerminateClick(Sender: TObject);
begin
  Tag := 1;
  Close;
end;

procedure TFormMain.ButtonDeleteClick(Sender: TObject);
begin
  if ListViewWindows.ItemIndex = -1 then
    Exit;
  KnownWindows.Delete(ListViewWindows.ItemIndex);
  ListViewWindows.Items.Count := KnownWindows.Count;
  ListViewWindows.Refresh;
  EnableEdits(False);
  ClearEdits;
end;

procedure TFormMain.EditClassWordsChange(Sender: TObject);
begin
  if LoadingEdits = 0 then
    SaveEdits(SelectedKnown);
end;

procedure TFormMain.ListViewWindowsData(Sender: TObject; Item: TListItem);
var
  Known: TKnownWindow;
begin
  Known := KnownWindows[Item.Index] as TKnownWindow;
  Item.Caption := Known.ClassWords;
  Item.SubItems.Add(Known.TitleWords);
  Item.SubItems.Add(IntToStr(Known.Monitor) + '/' + IntToStr(Known.Position));
end;

function TFormMain.SelectedKnown: TKnownWindow;
begin
  if ListViewWindows.ItemIndex <> -1 then
    Result := KnownWindows[ListViewWindows.ItemIndex] as TKnownWindow
  else
    Result := nil;
end;

procedure TFormMain.EditTitleWordsChange(Sender: TObject);
begin
  if LoadingEdits = 0 then
    SaveEdits(SelectedKnown);
end;

procedure TFormMain.ComboBoxMonitorChange(Sender: TObject);
begin
  if LoadingEdits = 0 then
    SaveEdits(SelectedKnown);
end;

procedure TFormMain.ComboBoxPositionChange(Sender: TObject);
begin
  if LoadingEdits = 0 then
    SaveEdits(SelectedKnown);
end;

procedure TFormMain.ButtonLookupClick(Sender: TObject);
begin
  Color := clGray;
  Lookuping := True;
end;

procedure TFormMain.FormActivate(Sender: TObject);
begin
  Color := clBtnFace;
  Lookuping := False;
end;

procedure TFormMain.ButtonApplyClick(Sender: TObject);
begin
  Repos;
end;

procedure TFormMain.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #27 then
  begin
    Key := #0;
    Close;
  end;
end;

end.

