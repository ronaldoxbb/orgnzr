program Orgnzr;

uses
  Forms,
  UnitFormMain in 'UnitFormMain.pas' {FormMain},
  UnitModels in 'UnitModels.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.ShowMainForm := False;
  Application.CreateForm(TFormMain, FormMain);
  Application.Run;
end.
