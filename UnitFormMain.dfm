object FormMain: TFormMain
  Left = 434
  Top = 159
  BorderStyle = bsDialog
  Caption = 'Orgnzr'
  ClientHeight = 432
  ClientWidth = 395
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  DesignSize = (
    395
    432)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 248
    Width = 59
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Class words:'
  end
  object Label2: TLabel
    Left = 8
    Top = 296
    Width = 54
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Title words:'
  end
  object Label3: TLabel
    Left = 8
    Top = 344
    Width = 38
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Monitor:'
  end
  object Label4: TLabel
    Left = 160
    Top = 344
    Width = 40
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Position:'
  end
  object ListViewWindows: TListView
    Left = 8
    Top = 8
    Width = 377
    Height = 225
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        AutoSize = True
        Caption = 'Class words'
      end
      item
        AutoSize = True
        Caption = 'Title words'
      end
      item
        Caption = 'Monitor/Position'
        Width = 120
      end>
    OwnerData = True
    ReadOnly = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
    OnData = ListViewWindowsData
    OnSelectItem = ListViewWindowsSelectItem
  end
  object EditClassWords: TEdit
    Left = 8
    Top = 264
    Width = 297
    Height = 21
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 1
    Text = 'EditClassWords'
    OnChange = EditClassWordsChange
  end
  object EditTitleWords: TEdit
    Left = 8
    Top = 312
    Width = 297
    Height = 21
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 2
    Text = 'EditTitleWords'
    OnChange = EditTitleWordsChange
  end
  object ComboBoxMonitor: TComboBox
    Left = 8
    Top = 360
    Width = 145
    Height = 21
    Style = csDropDownList
    Anchors = [akLeft, akBottom]
    ItemHeight = 13
    TabOrder = 3
    OnChange = ComboBoxMonitorChange
  end
  object ComboBoxPosition: TComboBox
    Left = 160
    Top = 360
    Width = 145
    Height = 21
    Style = csDropDownList
    Anchors = [akLeft, akBottom]
    ItemHeight = 13
    TabOrder = 4
    OnChange = ComboBoxPositionChange
  end
  object ButtonNew: TButton
    Left = 8
    Top = 392
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '&New'
    TabOrder = 5
    OnClick = ButtonNewClick
  end
  object ButtonDelete: TButton
    Left = 88
    Top = 392
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '&Delete'
    TabOrder = 6
    OnClick = ButtonDeleteClick
  end
  object ButtonTerminate: TButton
    Left = 312
    Top = 392
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Terminate'
    TabOrder = 7
    OnClick = ButtonTerminateClick
  end
  object ButtonLookup: TButton
    Left = 312
    Top = 264
    Width = 75
    Height = 21
    Anchors = [akRight, akBottom]
    Caption = '&Lookup...'
    TabOrder = 8
    OnClick = ButtonLookupClick
  end
  object ButtonApply: TButton
    Left = 312
    Top = 360
    Width = 75
    Height = 21
    Caption = '&Apply'
    TabOrder = 9
    OnClick = ButtonApplyClick
  end
  object TimerRepos: TTimer
    Interval = 400
    OnTimer = TimerReposTimer
    Left = 24
    Top = 16
  end
end
