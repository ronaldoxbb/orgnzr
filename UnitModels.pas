unit UnitModels;

interface

uses
  SysUtils, Classes, Contnrs, Types;

type
  TKnownWindow = class
  public
    ClassWords, TitleWords: string;
    Monitor: Integer;
    Position: Integer;
  end;

  TWindowLayout = class;

  TWindowLayoutCell = class
  private
    OwnerLayout: TWindowLayout;
  public
    Name: string; 
    RowIndex: Integer;
    ColIndex: Integer;
    RowSpan: Integer;
    ColSpan: Integer;
    constructor Create(AOwnerLayout: TWindowLayout; AName: string; ARowIndex, AColIndex, ARowSpan, AColSpan: Integer);
    function Rect(AFullRect: TRect): TRect;
    function Index: Integer;
  end;

  TWindowLayout = class
  private
    FRowCount: Integer;
    FColCount: Integer;
    FCells: TObjectList;
    function GetCellCount: Integer;
    function GetCells(I: Integer): TWindowLayoutCell;
  public
    constructor Create(ARowCount, AColCount: Integer); virtual;
    destructor Destroy; override;
    function BestCellForRect(AFullRect, ASomeRect: TRect): TWindowLayoutCell;
    property Cells[I: Integer]: TWindowLayoutCell read GetCells;
    property CellCount: Integer read GetCellCount;
    property RowCount: Integer read FRowCount;
    property ColCount: Integer read FColCount;
  end;

function NewLayout3: TWindowLayout;

implementation

function RectArea(R: TRect): Integer;
begin
  Result := (R.Right - R.Left) * (R.Bottom - R.Top);
end;

function NewLayout3: TWindowLayout;
begin
  Result := TWindowLayout.Create(3, 2);
  Result.FCells.Add(TWindowLayoutCell.Create(Result, 'left-top', 0, 0, 2, 1));
  Result.FCells.Add(TWindowLayoutCell.Create(Result, 'right-top', 0, 1, 2, 1));
  Result.FCells.Add(TWindowLayoutCell.Create(Result, 'bottom', 2, 0, 1, 2));
end;

{ TWindowLayout }

function TWindowLayout.BestCellForRect(AFullRect, ASomeRect: TRect): TWindowLayoutCell;
var
  I: Integer;
  MaxArea, MaxIndex, Area: Integer;
  CellRect, Inter: TRect;
begin
  MaxIndex := -1;
  MaxArea := 0;
  for I := 0 to CellCount - 1 do
  begin
    Result := Cells[I];
    CellRect := Result.Rect(AFullRect);
    if IntersectRect(Inter, ASomeRect, CellRect) then
    begin
      Area := RectArea(Inter);
      if (MaxIndex = -1) or (Area > MaxArea) then
      begin
        MaxIndex := I;
        MaxArea := Area;
      end;
    end;
  end;
  if MaxIndex <> -1 then
    Result := Cells[MaxIndex]
  else
    Result := nil;
end;

constructor TWindowLayout.Create(ARowCount, AColCount: Integer);
begin
  inherited Create;
  FRowCount := ARowCount;
  FColCount := AColCount;
  FCells := TObjectList.Create;
end;

destructor TWindowLayout.Destroy;
begin
  FCells.Free;
  inherited;
end;

function TWindowLayout.GetCellCount: Integer;
begin
  Result := FCells.Count;
end;

function TWindowLayout.GetCells(I: Integer): TWindowLayoutCell;
begin
  Result := FCells[I] as TWindowLayoutCell;
end;

{ TWindowLayoutCell }

constructor TWindowLayoutCell.Create(AOwnerLayout: TWindowLayout; AName: string; ARowIndex, AColIndex, ARowSpan,
  AColSpan: Integer);
begin
  inherited Create;
  Self.OwnerLayout := AOwnerLayout;
  Self.Name := AName;
  Self.RowIndex := ARowIndex;
  Self.ColIndex := AColIndex;
  Self.RowSpan := ARowSpan;
  Self.ColSpan := AColSpan;
end;

function TWindowLayoutCell.Index: Integer;
begin
  Result := OwnerLayout.FCells.IndexOf(Self);
end;

function TWindowLayoutCell.Rect(AFullRect: TRect): TRect;
var
  FullWidth, FullHeight: Integer;
begin
  FullWidth := AFullRect.Right - AFullRect.Left;
  FullHeight := AFullRect.Bottom - AFullRect.Top;
  Result.Left := AFullRect.Left + Trunc(FullWidth * Self.ColIndex / OwnerLayout.ColCount);
  Result.Top := AFullRect.Top + Trunc(FullHeight * Self.RowIndex / OwnerLayout.RowCount);
  Result.Right := Result.Left + Trunc(FullWidth * Self.ColSpan / OwnerLayout.ColCount);
  Result.Bottom := Result.Top + Trunc(FullHeight * Self.RowSpan / OwnerLayout.RowCount);
end;

end.
